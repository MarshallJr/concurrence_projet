# Concurrence_projet
Projet de "Programmation objet" réalisé à l'IUT de Nantes

##### Sujet : Concurrence (écriture/lecture)

## Déclaration :
- Université :  IUT de Nantes  
- Groupe-classe : LP MiAR Mixte   
- Année 2019 - 2020
- Repertoire racine (root) du projet : concurrence_projet

##### Avancement : non-terminé/en cours

## Développeurs : 
- Théo Coubard  
- Loïs Viaud

## Informations Générales :
Ce projet est un indexeur de contenu (google search minimaliste), le programme s'occupe de rechercher une occurence du mot passé en paramètre dans la page (qui lui est aussi donnée en paramètre) et ensuite, une fois que la lecture est terminée et s'il a trouvé le mot dans la page, le programme commence la même recherche dans les pages liées à la première (via les balise de lien "a" [anchor]).  

Le programme repose sur une pool de plusieurs threads (nombre définissable via commande) pour fonctionner et intègre donc des technologies spécifiques pour l'écriture concurrente de plusieurs thread dans un fichier ou l'appel à un méthode d'un objet conteneur de données (verrous), il intègre aussi des technologies pour la lecture et l'analyse de page HTML (jsoup).  

La technologie jsoup utilisée provient d'une entité extérieure au projet et à l'université.  
Page officiel de cette technologie : https://jsoup.org/

Le sujet du projet est aussi contenu dans le dossier "extra" du projet.
Une archive java (jar) est disponible dans le dossier  "docs" du projet.

## Langages : 
- Java
- HTML/CSS (fichiers/pages lus)

## Conditions de lancement et logiciels nécessaires
- Java (jdk/jre)
- IntelliJ IDEA

## Lancement du programme (projet IntelliJ IDEA)
- Ouvrir le projet
- Créer une configuration pour le lancement (Main class : "main", Program arguments : [par défaut] "Nantes https://fr.wikipedia.org/wiki/Nantes 100", ...)
- Lancer le programme via le bouton prévu à cet effet (triangle vert : "Run", raccourcie : Maj+F10)

## Lancement du programme (jar) :
- Se déplacer dans le répertoire du projet à lancer : dossier "{racine}\out\artifacts\concurrence_projet_jar/"
- Lancer l'application avec la commande :  
Avec les arguments de votre choix :
```shell
    java -jar ./concurrence_projet.jar _mot à chercher_ _lien vers la première page_ _nombre de thread maximal_
```
Avec les arguments par défaut :
```shell
    java -jar ./concurrence_projet.jar
```

# Liens / Webographie (non éxhaustive)
- Java : https://www.java.com/fr/download/
- Java (documentation) : https://docs.oracle.com/javase/8/docs/api/
- Jsoup : https://jsoup.org/
- Jsoup (documentation) : https://jsoup.org/apidocs/overview-summary.html