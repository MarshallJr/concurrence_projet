package PageIndexer;

import java.net.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Classe principale
 * La méthode prendra des arguments par défaut si ceux-ci ne sont pas spécifiés lors de l'appel.
 * argument 1 : mot à chercher (par défaut : Nantes [String])
 * argument 2 : hyperlien (racine) (par défaut : https://fr.wikipedia.org/wiki/Nantes [String])
 * argument 3 : nombre de thread maximal (par défaut : 10 [int])
 */
public class main {
    public static void main(String[] args)
    {
        if (args.length <= 3) {
            Index index = new Index();


            String toSearch;
            if (args.length > 0 && args[0] != null) {
                toSearch = args[0];
            } else {
                //par défaut
                toSearch = "Nantes";
            }

            boolean ok = true;
            URL url = null;
            if ( args.length > 1 && args[1] != null) {
                try {
                    url = new URL(args[1]);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    ok = false;
                    System.out.println("L'url est mal formé, veuillez le vérifier !");
                }
            } else {
                //par défaut
                try {
                    url = new URL("https://fr.wikipedia.org/wiki/Nantes");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    ok = false;
                    System.out.println("Une erreur est survenue lors de l'utilisation de l'url par défaut !");
                }
            }

            if (ok) {
                int maxThreadNumber = 0;
                if (args.length > 2 && args[2] != null) {
                    try {
                        maxThreadNumber = Integer.parseInt(args[2]);
                    } catch(NumberFormatException e) {
                        e.printStackTrace();
                        System.out.println("Le nombre de thread spécifié est incorrect !");
                    }
                } else {
                    //par défaut
                    maxThreadNumber = 10;
                }

                if (ok && toSearch.length() > 0 && url.toString().length() > 0 && maxThreadNumber > 0) {
                    ThreadPoolExecutor TPE = (ThreadPoolExecutor) Executors.newFixedThreadPool(maxThreadNumber);
                    TPE.execute(new SearchRunnable(TPE, index, url, toSearch));
                } else {
                    System.out.println("Une erreur est survenue dans le lancement du programme, veuillez vérifier les arguments et réessayer !");
                }
            } else {
                System.out.println("Une erreur est survenue dans l'obtention des paramètres de lancement, veuillez vérifier vos arguments ou les arguments par défaut !");
            }
        }

        /*if (args.length != 3) {
            System.out.println("Error : invalid arguments number or arguments type");
        } else {
            String toSearch = args[0];
            try {
                int maxThreadNumber = Integer.parseInt(args[2]);
                url = new URL(args[1]);
                ThreadPoolExecutor TPE = (ThreadPoolExecutor) Executors.newFixedThreadPool(maxThreadNumber);
                TPE.execute(new SearchRunnable(TPE, index, url, toSearch));
            } catch (NumberFormatException e) {
                System.out.println("Invalid max thread number format");
            } catch (MalformedURLException e) {
                System.out.println("Malformed URL");
            }
        }*/
    }
        /*try {
            URL oracle = new URL("https://fr.wikipedia.org/wiki/Nantes");
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(oracle.openStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null)
                System.out.println(inputLine);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/
}
