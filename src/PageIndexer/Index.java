package PageIndexer;

import java.net.URL;
import java.util.HashSet;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Classe d'indexation des URLs lus
 */
class Index {
    private HashSet<URL> index;
    private ReentrantReadWriteLock RRWL = new ReentrantReadWriteLock();
    private Lock readLock = RRWL.readLock();
    private Lock writeLock = RRWL.writeLock();

    /**
     * Construteur, ne nécessite pas de paramètre en entré
     */
    Index() {
        index = new HashSet<>();
    }

    /**
     * Vérifie si la page liée à l'URL passé en paramètre a déjà été vérifié
     * @param pageURL l'URL de la page à vérifier
     * @return true si la page liée à l'URL a déjà été vérifié, false sinon
     */
    boolean verify(URL pageURL) {
        System.out.println("Verifying if the page " + pageURL + " has already been caught.");
        this.readLock.lock();
        try {
            return (this.index.contains(pageURL));
        } finally {
            this.readLock.unlock();
        }
    }

    /**
     * Écrit l'URL de la page dans l'index
     * @param pageURL l'URL de la page à ajouter à l'index
     */
    void write(URL pageURL) {
        if (!verify(pageURL)) {
            System.out.println("The page at " + pageURL + " will be verified/scanned.");
            this.writeLock.lock();
            try {
                //Thread.sleep(1000);
                this.index.add(pageURL);
                System.out.println(pageURL + " has been written");
            } /*catch (InterruptedException e) {
                e.printStackTrace();
                this.index.add(pageURL);
            }*/ finally {
                this.writeLock.unlock();
            }
        }
    }
}
